import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from '../services/company.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit {
  companies: any;
  columnForm: any;
  clientId: any;

  constructor(
    private activateRoute: ActivatedRoute,
    private companyService: CompanyService,
    private modalService: NgbModal,
    private fb:FormBuilder,
    private router:Router
  ) { }

  ngOnInit(): void {

    this.activateRoute.paramMap.subscribe((params) => {
      const id = params.get('clientId');
      console.log(id)
      this.clientId=id;
      this.createColumnForm()
      this.getCompanies(id);
    });
  }

  createColumnForm() {
    this.columnForm = this.fb.group({
      id: 0,
      name: [''],
      clientId:this.clientId
    });
  }

  getCompanies(id:any){
    this.companyService.getByClientId(id).subscribe(data=>{
      this.companies=data;
      console.log(data)
    })
  }

  openColumnCreateModal(modal:any) {

    this.modalService.open(modal, { size: 'lg' });
  }

  createColumn(){
    console.log(this.columnForm.value)
    this.companyService.createNewProperty(this.columnForm.value).subscribe(data=>{
      document.getElementById("close-button")?.click()
      this.getCompanies(this.clientId)

    })
  }

  editCompany(id:any){
    this.router.navigate(['company-edit',id]);
  }

  createCompany(){
    this.router.navigate(['company-create',this.clientId]);
  }



}
