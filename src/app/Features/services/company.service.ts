import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, Subject, throwError } from 'rxjs';

import { environment } from 'src/environments/environment';
const httpOptions = {
  headers: new HttpHeaders({
    // 'Content-Type': 'application/json',
    // Accept: 'application/json',
    // Authorization: 'Bearer ' + localStorage.getItem('lmsToken'),
  }),
};
@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private httpClient: HttpClient) {}

  baseUrl = environment.apiUrl;



   createNewProperty(model: any): Observable<any> {
    return this.httpClient
      .post<any>(this.baseUrl + 'ExtraCompanyProperty', model, {
        headers: httpOptions.headers,
      })
      .pipe(catchError(this.handleError));
  }

  updateCompany(data:any): Observable<any> {
    return this.httpClient
      .put<any>(this.baseUrl + 'Company/' + data.id, data)
      .pipe(catchError(this.handleError));
  }
  createCompany(data:any): Observable<any> {
    return this.httpClient
      .post<any>(this.baseUrl + 'Company', data)
      .pipe(catchError(this.handleError));
  }

  getById(id: string): Observable<any> {
    return this.httpClient
      .get<any>(this.baseUrl + 'Company/' + id)
      .pipe(catchError(this.handleError));
  }

  getByClientId(id: any): Observable<any> {
    return this.httpClient
      .get<any>(this.baseUrl + 'Company/GetCompnaiesForCLient?clientId=' + id)
      .pipe(catchError(this.handleError));
  }




  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.error('Client Side Error: ', errorResponse);
    } else {
      console.error('Server Side error', errorResponse);
    }
    return throwError(errorResponse);
  }
}
