import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from '../services/company.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss']
})
export class CompanyEditComponent implements OnInit {
  companyId: any;
  companyForm: any;
  company: any;
  propertyValues: any;
  clientId: any;

  constructor(
    private activateRoute: ActivatedRoute,
    private companyService: CompanyService,
    private fb:FormBuilder,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.activateRoute.paramMap.subscribe((params) => {
      const id = params.get('id');
      this.clientId = params.get('clientId');
      console.log(id)
      this.companyId=id;
      this.createCompanyForm()
      this.getCompanyById(id);
    });
  }

  getCompanyById(id: any){
    this.companyService.getById(id).subscribe(data=>{
      this.company=data
      console.log(data)
      this.setCompanyToform(data)
    })
  }

  createCompanyForm() {
    this.companyForm = this.fb.group({
      id: 0,
      name: [''],
      address:null,
      clientId:this.clientId,
      propertyValues:null
  })}

  setCompanyToform(data:any){
this.companyForm.patchValue({
  id:data.id,
  name:data.name,
  address:data.address,
  clientId:data.clientId,
  propertyValues:null
})
this.propertyValues=[];
data.client?.extraCompanyProperties.forEach((element:any) => {
  var value=data.propertyValues.find((c:any)=>c.extraCompanyPropertyId==element.id)
  var property={
    id:0,
    extraCompanyPropertyId:element.id,
    companyId:data.id,
    value:value?value.value:"",
    name:element.name

  }
  this.propertyValues.push(property)
});
  }


  setPropertyValue(event:any,i:any){
    this.propertyValues[i].value=event.target.value
  }

  uploadForm(){
    var company=this.companyForm.value
    company.propertyValues=this.propertyValues
    console.log(company)
    if(this.companyId>0){
      this.companyService.updateCompany(company).subscribe(data=>{
        this.router.navigate(['companyList',company.clientId]);
      })
    }else{
      this.companyService.createCompany(company).subscribe(data=>{
        this.router.navigate(['companyList',company.clientId]);
      })
    }
   
  }

}
