import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './Features/home/home.component';
import { CompanyListComponent } from './Features/company-list/company-list.component';
import { ClientComponent } from './Features/client/client.component';
import { CompanyEditComponent } from './Features/company-edit/company-edit.component';

const routes: Routes = [ { path: '', component: ClientComponent },

// { path: 'home', component: HomepageComponent, data: { breadcrumbs: 'হোম' } },

{
  path: 'client',
  component: ClientComponent,
},
{
  path: 'companyList/:clientId',
  component: CompanyListComponent,
},
{
  path: 'company-edit/:id',
  component: CompanyEditComponent,
},
{
  path: 'company-create/:clientId',
  component: CompanyEditComponent,
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
